package com.tomplese.kafka_producer.model;


public class SimpleModel {
    private String fieldOne;
    private String fieldTwo;

    public SimpleModel(){}

    public SimpleModel(String fieldOne, String fieldTwo){
        this.fieldOne = fieldOne;
        this.fieldTwo = fieldTwo;
    }

    public String getFieldOne() {
        return fieldOne;
    }

    public void setFieldOne(String fieldOne) {
        this.fieldOne = fieldOne;
    }

    public String getFieldTwo() {
        return fieldTwo;
    }

    public void setFieldTwo(String fieldTwo) {
        this.fieldTwo = fieldTwo;
    }
}
